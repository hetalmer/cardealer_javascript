
function bmwAndAudi(inventory = undefined) {
    if (inventory && Array.isArray(inventory)) {
        bmwaudiArray = [];
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i]['car_make'] === 'BMW' || inventory[i]['car_make'] === 'Audi') { // get only BMW or Audi car
                bmwaudiArray.push(inventory[i]);
            }
        }
        return bmwaudiArray;
    }
    return [];

}
module.exports = bmwAndAudi;
