function carYear(inventory) {
    if (inventory && Array.isArray(inventory)) {
        let yearArray = [];
        for (let i = 0; i < inventory.length; i++) {
            yearArray[i] = inventory[i]['car_year'];//get the car_year ans store it in the array
        }
        return yearArray;
    }
    return [];

}
module.exports = carYear;