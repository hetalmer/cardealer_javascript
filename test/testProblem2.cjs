// Display the inforamtion about last car in inventory
const lastCar = require('../problem2.cjs');
let inventory = require('../inventory.json');
output = lastCar(inventory);
console.log(`Last car is a *${output.car_make}* *${output.car_model}`);
