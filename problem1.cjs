function searchById(inventory = undefined, searchId = undefined) {
    if (inventory && Array.isArray(inventory) && Number.isInteger(searchId) && searchId >= 0) {
        //  validation for arguments
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id === searchId) { // find the searchId and return it to where it is called
                return inventory[i];
            }
        }
    }
    return [];
}
module.exports = searchById;
