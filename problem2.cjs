function lastCar(inventory = undefined) {
    if (inventory && Array.isArray(inventory)) {
        return inventory[inventory.length - 1];//get last record from inventory
    }
    return []
}
module.exports = lastCar;
