function sortInventory(inventory = undefined) {
    if (inventory && Array.isArray(inventory)) { // validate
        let carModels = [];
        for (let i = 0; i < inventory.length; i++) {
            carModels.push(inventory[i]['car_model'])
        }
        return carModels.sort(); // after getting detail of car models, sorting it into alphabetical order
    }
    return [];

}
module.exports = sortInventory;